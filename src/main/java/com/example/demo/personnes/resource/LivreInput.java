package com.example.demo.personnes.resource;

public class LivreInput {
    Long idLivre;

    public LivreInput() {
        super();
    }

    public Long getIdLivre() {
        return idLivre;
    }

    public void setIdLivre(Long idLivre) {
        this.idLivre = idLivre;
    }
}
